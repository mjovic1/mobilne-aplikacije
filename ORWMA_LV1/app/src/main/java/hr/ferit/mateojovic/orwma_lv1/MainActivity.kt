package hr.ferit.mateojovic.orwma_lv1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.core.text.isDigitsOnly

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        val editText2 = findViewById<EditText>(R.id.editTextTextPersonName12)
        val editText3 = findViewById<EditText>(R.id.editTextTextPersonName13)

        findViewById<Button>(R.id.button3).setOnClickListener {
            if (!editText2.text.toString().isDigitsOnly() or !editText3.text.toString()
                    .isDigitsOnly()
            )
                Toast.makeText(
                    applicationContext, "Unesite brojeve",
                    Toast.LENGTH_LONG
                ).show()
            else {
                val index = editText3.text.toString().toFloat() / ((editText2.text.toString()
                    .toFloat() / 100) * (editText2.text.toString().toFloat() / 100))
                Toast.makeText(
                    applicationContext, index.toString(),
                    Toast.LENGTH_LONG
                ).show()
            }

        }


    }
}