package hr.ferit.mateojovic.myapplication

import android.media.AudioManager
import android.media.SoundPool
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import hr.ferit.mateojovic.myapplication.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {


    lateinit var binding: ActivityMainBinding
    private lateinit var mSoundPool: SoundPool
    private var mLoaded: Boolean = false
    var mSoundMap: HashMap<Int, Int> = HashMap()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setUpUi()
        loadSounds()
    }
    private fun setUpUi() {
        binding.button1.setOnClickListener {playSound(R.raw.airplane)}
        binding.button2.setOnClickListener {playSound(R.raw.heavy)}
        binding.button3.setOnClickListener {playSound(R.raw.service)}
    }
    private fun loadSounds() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            this.mSoundPool = SoundPool.Builder().setMaxStreams(10).build()
        } else {
            this.mSoundPool = SoundPool(10, AudioManager.STREAM_MUSIC, 0)
        }
        this.mSoundPool.setOnLoadCompleteListener { _, _, _ -> mLoaded = true }
        this.mSoundMap[R.raw.airplane] = this.mSoundPool.load(this, R.raw.airplane, 1)
        this.mSoundMap[R.raw.heavy] = this.mSoundPool.load(this, R.raw.heavy, 1)
        this.mSoundMap[R.raw.service] = this.mSoundPool.load(this, R.raw.service, 1)

    }


    fun playSound(selectedSound: Int) {
        val soundID = this.mSoundMap[selectedSound] ?: 0
        this.mSoundPool.play(soundID, 1f, 1f, 1, 0, 1f)
    }



}