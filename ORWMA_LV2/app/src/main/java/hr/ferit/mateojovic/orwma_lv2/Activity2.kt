package hr.ferit.mateojovic.orwma_lv2

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class Activity2 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_2)

        findViewById<Button>(R.id.button).setOnClickListener {

            MainActivity();
        }
    }

    fun MainActivity() {
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
    }
}